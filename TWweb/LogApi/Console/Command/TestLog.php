<?php

namespace TWweb\LogApi\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestLog extends Command
{

    protected function configure()
    {
        $this->setName('testlog:test')
        ->setDescription('FirstTest');
        parent::configure();
    }

    protected
    function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(('LogApi Twweb'));
    }
}
